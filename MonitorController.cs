﻿using System.Windows;
using System;

namespace MicrophoneControl
{
    public static class Monitors
    {
        public static void TestMe()
        {
            SpawnPoint.MonitorInfo.Reload();

            foreach (var monitor in SpawnPoint.MonitorInfo.Monitors)
            {
                Console.WriteLine("{0} | Rozlišení: {1} | Pracovní plocha: {2}", monitor.DisplayName, monitor.Bounds.Size, monitor.WorkArea.Size);
            }
        }

        public static Monitor Primary { get { return new Monitor(System.Windows.Forms.Screen.PrimaryScreen); } }

        public static Monitor GetFromWindow(Window wnd)
        {
            var screen = System.Windows.Forms.Screen.FromHandle(new System.Windows.Interop.WindowInteropHelper(wnd).Handle);
            return new Monitor(screen);
        }

        public static Monitor GetFromIndex(int monitorIndex)
        {
            var all = System.Windows.Forms.Screen.AllScreens;
            if (monitorIndex < 0 || monitorIndex >= all.Length) { return null; }

            return new Monitor(all[monitorIndex]);
        }

        public class Monitor
        {
            private System.Windows.Forms.Screen associatedScreen;

            public Area Bounds { get { return new Area(associatedScreen.Bounds); } }
            public Area WorkingArea { get { return new Area(associatedScreen.WorkingArea); } }

            public enum Corners
            {
                TopLeft,
                TopRight,
                BottomLeft,
                BottomRight
            }

            public Monitor(System.Windows.Forms.Screen scr)
            {
                associatedScreen = scr;
            }

            public void MoveWindowToCorner(Window wnd, Corners corner)
            {
                var newPoint = GetPointOfCorner(corner);

                if (corner == Corners.BottomLeft || corner == Corners.BottomRight)
                {
                    wnd.Top = newPoint.Y - wnd.Height;

                    if (corner == Corners.BottomRight)
                    {
                        wnd.Left = newPoint.X - wnd.Width;
                    }
                    else if (corner == Corners.BottomLeft)
                    {
                        wnd.Left = newPoint.X;
                    }
                }
                else if (corner == Corners.TopLeft || corner == Corners.TopRight)
                {
                    wnd.Top = newPoint.Y;

                    if (corner == Corners.TopRight)
                    {
                        wnd.Left = newPoint.X - wnd.Width;
                    }
                    else if (corner == Corners.TopLeft)
                    {
                        wnd.Left = newPoint.X;
                    }
                }
            }

            public Point GetPointOfCorner(Corners corner)
            {
                double x = 0;
                double y = 0;

                if (corner == Corners.BottomLeft || corner == Corners.BottomRight)
                {
                    y = WorkingArea.Y + WorkingArea.Height;
                }
                else if (corner == Corners.TopLeft || corner == Corners.TopRight)
                {
                    y = WorkingArea.Y;
                }

                if (corner == Corners.TopLeft || corner == Corners.BottomLeft)
                {
                    x = WorkingArea.X;
                }
                else if (corner == Corners.TopRight || corner == Corners.BottomRight)
                {
                    x = WorkingArea.X + WorkingArea.Width;
                }

                return new Point(x, y);
            }

            public struct Area
            {
                public int X;
                public int Y;
                public int Width;
                public int Height;

                public Area(System.Drawing.Rectangle rect)
                {
                    X = rect.X;
                    Y = rect.Y;
                    Width = rect.Width;
                    Height = rect.Height;
                }
            }
        }
    }
}
