﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Linq;

namespace SpawnPoint
{
    //TODO: ODSTRANIT System.Windows.Forms :DDD

    public static class MonitorInfo
    {
        static List<DisplayInfo> displays = new List<DisplayInfo>();
        static List<DisplayInfo> monitors = new List<DisplayInfo>();

        public static int NumberOfMonitors { get { return monitors.Count; } }

        public static DisplayInfo[] Monitors { get { return monitors.ToArray(); } }

        public static void Reload()
        {
            displays.Clear();

            var screens = GetScreens();
            var connectables = GetDevices();

            if (connectables != null)
            {
                foreach (var port in connectables)
                {
                    bool isConnected = ((port.StateFlags & WinApi.DisplayDeviceStateFlags.AttachedToDesktop) != 0);
                    bool isPrimary = ((port.StateFlags & WinApi.DisplayDeviceStateFlags.PrimaryDevice) != 0);
                    string devName = port.DeviceName;
                    var asS = (from scr in screens where scr.szDevice == devName select scr);
                    var screen = (asS.Count() == 1 ? asS.FirstOrDefault() : (WinApi.MONITORINFOEX?)null);
                    bool gotScreen = (asS.Count() == 1);

                    //if ((isConnected && gotScreen))
                    //{
                    //    Console.WriteLine("COOL");
                    //}
                    //else if (!(isConnected && gotScreen))
                    //{
                    //    Console.WriteLine("NOT CONNECTED");
                    //}
                    //else
                    //{
                    //    Console.WriteLine("BSHIT !!!");
                    //}

                    if (isConnected == gotScreen)
                    {
                        var display = new DisplayInfo();
                        display.IsConnected = isConnected;
                        display.IsPrimary = isPrimary;
                        display.DisplayName = port.DeviceName;

                        if (screen.HasValue)
                        {
                            var sc = screen.Value;

                            display.Bounds = new DisplayInfo.Screen(sc.rcMonitor);
                            display.WorkArea = new DisplayInfo.Screen(sc.rcWork);
                        }

                        displays.Add(display);

                        if (screen.HasValue)
                        {
                            monitors.Add(display);
                        }
                    }
                }
            }
        }

        private static List<WinApi.DISPLAY_DEVICE> GetDevices()
        {
            List<WinApi.DISPLAY_DEVICE> devices = new List<WinApi.DISPLAY_DEVICE>();

            try
            {
                WinApi.DISPLAY_DEVICE display = new WinApi.DISPLAY_DEVICE();
                display.Init();

                for (uint id = 0; WinApi.EnumDisplayDevices(null, id, ref display, 0); id++)
                {
                    display.cb = Marshal.SizeOf(display);

                    if (id == 0)
                    {
                        display.StateFlags = WinApi.DisplayDeviceStateFlags.AttachedToDesktop;
                        display.DeviceName = @"\\.\DISPLAY1";
                    }

                    devices.Add(display);
                }
            }
            catch (Exception)
            {
                return null;
            }

            return devices;
        }

        private static List<WinApi.MONITORINFOEX> GetScreens()
        {
            List<WinApi.MONITORINFOEX> screens = new List<WinApi.MONITORINFOEX>();

            WinApi.EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, delegate (IntPtr hMonitor, IntPtr hdcMonitor, ref WinApi.RECT lprcMonitor, IntPtr dwData)
            {
                var mi = new WinApi.MONITORINFOEX();
                mi.Init();

                bool success = WinApi.GetMonitorInfo(hMonitor, ref mi);
                if (success)
                {
                    screens.Add(mi);
                }

                return true;
            }, IntPtr.Zero);

            return screens;
        }

        public class DisplayInfo
        {
            public bool IsConnected { get; set; }
            public bool IsPrimary { get; set; }
            public Screen Bounds { get; set; }
            public Screen WorkArea { get; set; }
            public string DisplayName { get; set; }

            public class Screen
            {
                public Point Location;
                public Size Size;

                internal Screen(WinApi.RECT rect)
                {
                    Location = rect.Location;
                    Size = rect.Size;
                }

                public override string ToString()
                {
                    return string.Format("{0}|{1}", Location, Size);
                }
            }
        }
    }

    class WinApi
    {
        [Flags()]
        internal enum DisplayDeviceStateFlags : int
        {
            /// <summary>The device is part of the desktop.</summary>
            AttachedToDesktop = 0x1,
            MultiDriver = 0x2,
            /// <summary>The device is part of the desktop.</summary>
            PrimaryDevice = 0x4,
            /// <summary>Represents a pseudo device used to mirror application drawing for remoting or other purposes.</summary>
            MirroringDriver = 0x8,
            /// <summary>The device is VGA compatible.</summary>
            VGACompatible = 0x10,
            /// <summary>The device is removable; it cannot be the primary display.</summary>
            Removable = 0x20,
            /// <summary>The device has more display modes than its output devices support.</summary>
            ModesPruned = 0x8000000,
            Remote = 0x4000000,
            Disconnect = 0x2000000
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        internal struct DISPLAY_DEVICE
        {
            [MarshalAs(UnmanagedType.U4)]
            public int cb;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string DeviceName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string DeviceString;
            [MarshalAs(UnmanagedType.U4)]
            public DisplayDeviceStateFlags StateFlags;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string DeviceID;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string DeviceKey;

            public void Init()
            {
                Marshal.SizeOf(this);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct RECT
        {
            /// <summary>
            /// The x-coordinate of the upper-left corner of the rectangle.
            /// </summary>
            public int Left;

            /// <summary>
            /// The y-coordinate of the upper-left corner of the rectangle.
            /// </summary>
            public int Top;

            /// <summary>
            /// The x-coordinate of the lower-right corner of the rectangle.
            /// </summary>
            public int Right;

            /// <summary>
            /// The y-coordinate of the lower-right corner of the rectangle.
            /// </summary>
            public int Bottom;

            public Point Location
            {
                get { return new Point(Left, Top); }
            }

            public Size Size
            {
                get { return new Size(Right - Left, Bottom - Top); }
            }
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        internal struct MONITORINFOEX
        {
            public int cbSize;
            public RECT rcMonitor;
            public RECT rcWork;
            public uint dwFlags;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szDevice;

            public void Init()
            {
                cbSize = 104;
                szDevice = string.Empty;
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        internal static extern bool GetMonitorInfo(IntPtr hMonitor, ref MONITORINFOEX lpmi);

        [DllImport("user32.dll")]
        internal static extern bool EnumDisplayDevices(string lpDevice, uint iDevNum, ref DISPLAY_DEVICE lpDisplayDevice, uint dwFlags);

        [DllImport("user32.dll")]
        internal static extern bool EnumDisplayMonitors(IntPtr hdc, IntPtr lprcClip, MonitorEnumDelegate lpfnEnum, IntPtr dwData);
        internal delegate bool MonitorEnumDelegate(IntPtr hMonitor, IntPtr hdcMonitor, ref RECT lprcMonitor, IntPtr dwData);
    }
}
