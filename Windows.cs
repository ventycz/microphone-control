﻿using System;
using System.Runtime.InteropServices;

namespace MicrophoneControl
{
    public static class Windows
    {
        const int GWL_STYLE = -16;
        const int GWL_EXSTYLE = (-20);
        const int WS_EX_TRANSPARENT = 0x20;
        const int WS_MAXIMIZEBOX = 0x10000;

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hwnd, int index);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hwnd, int index, int newStyle);

        public static void ClickThru(IntPtr hwnd)
        {
            dynamic actStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
            SetWindowLong(hwnd, GWL_EXSTYLE, actStyle | WS_EX_TRANSPARENT);
        }
    }
}
