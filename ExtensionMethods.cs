﻿using System;
using System.Windows;
using Shell = System.Windows.Shell;

namespace MicrophoneControl
{
    public static class ExtensionMethods
    {
        public static void Beautify(this Window wnd)
        {
            wnd.WindowStyle = WindowStyle.SingleBorderWindow;

            Shell.WindowChrome.SetWindowChrome(wnd, new Shell.WindowChrome
            {
                ResizeBorderThickness = new Thickness(0),
                GlassFrameThickness = new Thickness(-1),
                UseAeroCaptionButtons = false,
                CaptionHeight = 0,
                CornerRadius = new CornerRadius(0),
                NonClientFrameEdges = Shell.NonClientFrameEdges.None
            });
        }

        public static IntPtr GetHandle(this Window wnd)
        {
            return new System.Windows.Interop.WindowInteropHelper(wnd).Handle;
        }

        public static void MakeClickThrough(this Window wnd)
        {
            Windows.ClickThru(wnd.GetHandle());
        }
    }
}
