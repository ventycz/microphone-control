﻿using System;

using CoreAudioApi;
using System.Collections.Generic;

namespace MicrophoneControl
{
    class MicController
    {
        private MMDevice device;
        MMDeviceEnumerator enumerator;
        private MMDevice defaultRecordDevice;
        private MMDeviceCollection recordDevices;

        private Dictionary<string, MMDevice> allRecordDevices = new Dictionary<string, MMDevice>();
        public Dictionary<string, MMDevice>.KeyCollection AllRecordDevices { get { return allRecordDevices.Keys; } }

        public int Volume
        {
            get
            {
                return (int)Math.Round(device.AudioEndpointVolume.MasterVolumeLevelScalar * 100, 0);
            }
            set
            {
                device.AudioEndpointVolume.MasterVolumeLevelScalar = value / 100f;
            }
        }
        public float MasterPeak { get { return device.AudioMeterInformation.MasterPeakValue * 100; } }
        public int ChannelCount { get { return device.AudioMeterInformation.PeakValues.Count; } }
        public bool Muted { get { return device.AudioEndpointVolume.Mute; } set { device.AudioEndpointVolume.Mute = value; } }

        public event EventHandler InfoUpdate;

        public MicController()
        {
            enumerator = new MMDeviceEnumerator();
            Refresh();
            UseDefault();
        }

        public void Refresh()
        {
            defaultRecordDevice = enumerator.GetDefaultAudioEndpoint(EDataFlow.eCapture, ERole.eMultimedia);
            recordDevices = enumerator.EnumerateAudioEndPoints(EDataFlow.eCapture, EDeviceState.DEVICE_STATE_ACTIVE);

            allRecordDevices.Clear();
            Console.WriteLine("Device Listing");
            for (int i = 0; i < recordDevices.Count; i++)
            {
                MMDevice dvc = recordDevices[i];
                allRecordDevices.Add(dvc.FriendlyName, dvc);

                Console.WriteLine(dvc.FriendlyName);
            }
            Console.WriteLine("End Of Device Listing");
        }

        public void UseDefault()
        {
            device = defaultRecordDevice;
            device.AudioEndpointVolume.OnVolumeNotification += AudioNotification;
        }

        public void UseDevice(string deviceName)
        {
            if (allRecordDevices.ContainsKey(deviceName))
            {
                device = allRecordDevices[deviceName];
                device.AudioEndpointVolume.OnVolumeNotification += AudioNotification;
            }
        }

        private void AudioNotification(AudioVolumeNotificationData data)
        {
            Console.WriteLine("Volume: {0} | Muted: {1}", Volume, Muted);

            if (InfoUpdate != null) { InfoUpdate.Invoke(this, null); }
        }
    }
}
