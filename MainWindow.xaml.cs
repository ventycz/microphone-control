﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace MicrophoneControl
{
    public partial class MainWindow : Window
    {
        MicController mic;
        Hotkeys.Hotkey shortcut;

        public MainWindow()
        {
            InitializeComponent();

            Monitors.TestMe();

            Menu_Exit.Click += Menu_Exit_Click;

            shortcut = new Hotkeys.Hotkey(System.Windows.Input.Key.BrowserForward, System.Windows.Input.ModifierKeys.None);
            Hotkeys.RegisterHotkey(shortcut);
            shortcut.Triggered += Shortcut_Triggered;

            mic = new MicController();
            mic.InfoUpdate += delegate (object sender, EventArgs e) { UpdateInterface(); };

            MoveToLocation();
            LocationChanged += delegate (object sender, EventArgs e) { MoveToLocation(); };
            SourceInitialized += delegate (object sender, EventArgs e) { this.MakeClickThrough(); };

            FixMic();
            NotifyIcon.Visibility = Visibility.Visible;
        }

        private void Menu_Exit_Click(object sender, RoutedEventArgs e)
        {
            NotifyIcon.Icon = null;
            NotifyIcon.Dispose();
            Environment.Exit(0);
        }

        private void Shortcut_Triggered(object sender, EventArgs e)
        {
            mic.Muted = !mic.Muted;
        }

        private void MoveToLocation()
        {
            //Try to setup on second monitor
            var monitor = Monitors.GetFromIndex(1);
            if (monitor == null) { monitor = Monitors.GetFromIndex(0); }

            monitor.MoveWindowToCorner(this, Monitors.Monitor.Corners.BottomRight);

            //Monitors.GetFromIndex(1).MoveWindowToCorner(this, Monitors.Monitor.Corners.BottomRight);
        }

        private void FixMic()
        {
            mic.Volume = 85;
            mic.Muted = false;
            UpdateInterface();
        }

        private void UpdateInterface()
        {
            if (Dispatcher.CheckAccess())
            {
                Indicator_Volume.Text = string.Format("{0} %", mic.Volume);
                Indicator_Muted.Visibility = (mic.Muted ? Visibility.Visible : Visibility.Hidden);
                Indicator_Unmuted.Visibility = (!mic.Muted ? Visibility.Visible : Visibility.Hidden);
            }
            else
            {
                try { Dispatcher.Invoke(delegate () { UpdateInterface(); }); }
                catch (Exception) { }
            }
        }
    }
}
