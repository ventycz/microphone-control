﻿using NHotkey.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using NHotkey;

namespace MicrophoneControl
{
    public static class Hotkeys
    {
        private static Dictionary<string, Hotkey> _list = new Dictionary<string, Hotkey>();

        public static Exception RegisterHotkey(Hotkey shortcut)
        {
            string Name = GenerateName();
            shortcut._name = Name;

            try
            {
                HotkeyManager.Current.AddOrReplace(Name, shortcut.Key, shortcut.Modifiers, HotkeyPressed);

                _list.Add(Name, shortcut);

                return null;
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        private static void HotkeyPressed(object sender, HotkeyEventArgs e)
        {
            string Name = e.Name;
            if (!_list.ContainsKey(Name)) { Console.WriteLine("Invalid Name: {0}", Name); return; }

            e.Handled = true;
            _list[Name].Trigger();
        }

        /// <summary>
        /// Function for generating random names for hotkey
        /// </summary>
        /// <param name="length">Length of generated name</param>
        /// <returns>Generated name</returns>
        private static string GenerateName(int length = 8)
        {
            string result = string.Empty;

            while ((result == string.Empty || _list.ContainsKey(result)))
            {
                //The list of characters
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                //Randomizer instance
                Random random = new Random();
                //The generated string
                result = new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
            }

            return result;
        }

        public class Hotkey
        {
            public event EventHandler Triggered;

            internal string _name;
            private Key _key;
            private ModifierKeys _modifiers;

            public Key Key { get { return _key; } }
            public ModifierKeys Modifiers { get { return _modifiers; } }

            public Hotkey(Key key, ModifierKeys modifiers)
            {
                _key = key;
                _modifiers = modifiers;
            }

            internal void Trigger()
            {
                Console.WriteLine("Triggered '{0}'!", _name);

                if (Triggered != null) { Triggered.Invoke(this, null); }
            }

            public override string ToString()
            {
                return string.Format("{0} = {1}", _modifiers, _key);
            }
        }
    }
}
